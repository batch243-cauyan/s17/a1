/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/



	
	//first function here:

    function showUserProfile(){
        let fullname = prompt("Enter your Full Name: ");
        let age = prompt("Enter your age:");
        let location = prompt("Enter your current location:");

        console.log(`Hello, ${fullname}`);
        console.log(`You are ${age} years old.`);
        console.log(`You live in ${location}.`);

    }

    showUserProfile();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
    function showTopBands(){
        let myTop5Bands = ["1. The Beatles", "2. Metallica", "3. The Eagles", "4. L'arc~en~Ciel", "5. Eraserheads"]
        for (let i = 0; i<myTop5Bands.length; i++){
            console.log(myTop5Bands[i]);
        }
    }

    showTopBands();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

    function displayMyMovies(){
        let myTopMovies = {
            // practice with obkects
            top1 : {
                number: "1.",
                title: "The Godfather",
                ratings: "Rotten Tomatoes Rating: 97%"
            },
            top2 : {
                number: "2.",
                title: "The Godfather, Part II",
                ratings: "Rotten Tomatoes Rating: 96%"
            },
            top3 : {
                number: "3.",
                title: "Shawshank Redemption",
                ratings: "Rotten Tomatoes Rating: 91%"
            },
            top4 : {
                number: "4.",
                title: "To Kill A Mockingbird",
                ratings: "Rotten Tomatoes Rating: 93%"
            },
            top5 : {
                number: "5.",
                title: "Psycho",
                ratings: "Rotten Tomatoes Rating: 96%"
            }
        }

        //   values
        Object.values(myTopMovies).forEach(key => {
            console.log(`${key.number} ${key.title}\n${key.ratings}`);
          });

    }
    displayMyMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);
